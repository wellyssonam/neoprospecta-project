import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-menu-dropdown',
  templateUrl: './menu-dropdown.component.html',
  styleUrls: ['./menu-dropdown.component.css']
})
export class MenuDropdownComponent implements OnInit {

  @Input() btnName: string
  @Input() listItems: Array<string>
  @Input() itemActivated: 'string'
  @Output() public dateFromAppMenu = new EventEmitter<Object>();

  constructor() { }

  ngOnInit() {
  }

  selectItem = (dataItem) => {
    this.dateFromAppMenu.emit({objItem: dataItem})
  }

}
