import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-alert-msg',
  templateUrl: './alert-msg.component.html',
  styleUrls: ['./alert-msg.component.css']
})
export class AlertMsgComponent implements OnInit {

  @Input() clientName: string
  @Input() activeMessage: boolean

  constructor() { }

  ngOnInit() {
  }

}
