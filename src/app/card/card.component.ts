import { Component, OnInit, Input } from '@angular/core';
import { Client } from '../clients/models/client';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() clients: Client

  constructor() { }

  ngOnInit() {
  }

}
