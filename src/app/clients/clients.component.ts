import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClientService } from './services/client.service';
import { Component, OnInit } from '@angular/core';
import { Client } from './models/client';
import { fromStringWithSourceMap } from 'source-list-map';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  clients: Client[] = []
  objBtnOrder: Object = {}
  objBtnFilter: Object = {}
  public quantityAllClients: number
  pageCount: number = 1
  clientName: string
  public activeMessage: boolean = false

  constructor(
    private modalService: NgbModal,
    private clientService: ClientService,
    private route: ActivatedRoute,
  ) {
  }

  
  ngOnInit() {
    this.objBtnOrder = {
      name: 'Order by',
      itemActivated: 'option0',
      list: [
        {name: 'Id', value: 'option0', type: 'number', ariaLabel: 'id'},
        {name: 'Name', value: 'option1', type: 'string', ariaLabel: 'name'},
        {name: 'Age', value: 'option2', type: 'number', ariaLabel: 'age'},
        {name: 'City', value: 'option3', type: 'string', ariaLabel: 'city'}
      ]
    }
    this.objBtnFilter = {... this.objBtnOrder}
    this.objBtnFilter['name'] = 'Filter by'
    this.objBtnFilter['itemActivated'] = 'option4'
    this.objBtnFilter['list'] = [{name: 'None', value: 'option4', type: 'string', ariaLabel: 'none'}].concat(this.objBtnFilter['list'])
    this.showClients()

    this.verifySizeScreenOnce()
    this.getSizeScreen()

    
  }

  ngAfterContentInit() {
    this.route.queryParams
      .subscribe(response => {
        if (Object.keys(response).length) {
          this.clientName = response.name
          this.activeMessage = response.success
        }
      }); 
  }

  getSizeScreen = () => {
    window.addEventListener('resize', function () {
      var largura = window.innerWidth;
  
      if (largura >= 768) {
        $('.clients .table-clients').css('display', 'block')
        $('.clients .card-clients').css('display', 'none')
      } else {
        $('.clients .table-clients').css('display', 'none')
        $('.clients .card-clients').css('display', 'block')

      }
    });
  }
  
  

  verifySizeScreenOnce = () => {
    var largura = $(document).width();

    if (largura >= 768) {
      $('.clients .table-clients').css('display', 'block')
      $('.clients .card-clients').css('display', 'none')
    } else {
      $('.clients .table-clients').css('display', 'none')
      $('.clients .card-clients').css('display', 'block')
    }
  }

  orderObjClients = (type: string, key: string) => {
    if (type ==='number') {
      this.clients.sort((a, b) => a[key] - b[key])
    } if (type ==='string') {
      this.clients.sort((a, b) => (a[key] < b[key]) ? -1 : (a[key] > b[key]) ? 1 : 0)
    }
  }

  filterObjClients = (key: string) => {
    // console.log('>>>. filter')
  }

  showClients = () => {
    this.clientService.getClients().subscribe(resClients => {
      this.clients = []
      resClients.forEach(value => {
        const client: Client = {
          id: value.id,
          name: value.name,
          age: value.age,
          city: value.city
        }
        this.clients.push(client)
      });
      this.quantityAllClients = this.clients.length
    }, err => {
      console.error(err)
    });
  }

  editClick = () => {
    
  }

  receiverFeedbackOrderField = (data) => {
    this.orderObjClients(data.objItem.type, data.objItem.ariaLabel)
  }

  receiverFeedbackFilterField = (data) => {
    this.filterObjClients(data.objItem.ariaLabel)
  }
  
  receiverFeedbackClientForm = (data) => {
    this.filterObjClients(data.objItem.ariaLabel)
  }

}
