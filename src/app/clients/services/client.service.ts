import { Injectable } from '@angular/core';
import { Client } from '../models/client';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }
  url = 'http://private-92a969-processoseletivo1.apiary-mock.com/customers'
  url_update = 'https://private-92a969-processoseletivo1.apiary-mock.com/customers/'

  getClients = () => {
    return this.http.get<Client[]>(this.url)
  }

  updateClient = (client: Observable<Client>) => {
    let url = window.location.href.split("?")[1]
    let listParams = url.split("&")
    listParams = listParams[0].split('=')
    let id = listParams[1]
    return this.http.put(this.url_update + id, client);
  }
}
