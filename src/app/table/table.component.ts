import { Component, OnInit, Input } from '@angular/core';
import { Client } from '../clients/models/client';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  @Input() clients: Client

  constructor() { }

  ngOnInit() {
  }

}
