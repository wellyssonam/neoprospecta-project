import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Client } from '../clients/models/client'
import { ClientService } from '../clients/services/client.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {

  clientForm: FormGroup
  client: Client
  modeInsert: boolean = false
  router: Router
  @Output() public dateFromClientForm = new EventEmitter<Object>();
  
  constructor(
    private formBuilder: FormBuilder,
    private serviceClient: ClientService,
    private route: ActivatedRoute,
    router: Router
  ) {
    this.router = router
  }

  ngOnInit() {
    this.clientForm = this.formBuilder.group({
      name: ['', Validators.required],
      age: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(999)]],
      city: ['', Validators.required],
    })

    if(!this.modeInsert) {
      this.route.queryParams
        .subscribe(response => {
          console.log(response)
          this.loadClient(response)
        });
      
    }
  }

  loadClient = (client) => {
    this.clientForm.patchValue(client)
  }

  saveClient = () => {
    if(this.clientForm.invalid) {
      return
    }

    if(!this.modeInsert) {
      let client: Observable<Client> = this.clientForm.value;
      this.serviceClient.updateClient(client)
        .subscribe(
          response => this.handleSuccessEdit(response, client),
          err => console.error(err)
        );
    }
  }

  handleSuccessEdit = (response, client: Observable<Client>) => {
    client['success'] = true
    this.router.navigate(['/'], {queryParams: client})
  }

  BackToMainPage = () => {
    this.router.navigate(['/'])
  }

}
